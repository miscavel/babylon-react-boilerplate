import React from 'react';
import SceneComponent from './util/SceneComponent';
import SampleScene from './sample/scene/SampleScene';

const sampleScene = new SampleScene();

export default () => (
    <SceneComponent 
      antialias 
      adaptToDeviceRatio
      scene={sampleScene} // First scene to render
      id='App-game' 
    />
)